import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import { Menu } from 'semantic-ui-react'

export default class MenuBar extends Component {
  state = {}

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (
      <Menu stackable>
        <Menu.Item as={Link} to="/">
          <img src='/images/logo.png' />
        </Menu.Item>

        <Menu.Item as={Link} 
          to='/'
          name='Home'
          active={activeItem === 'features'}
          onClick={this.handleItemClick}
        >
          Home
        </Menu.Item>

        <Menu.Item
          name='Events'
          active={activeItem === 'testimonials'}
          onClick={this.handleItemClick}
        >
          Events
        </Menu.Item>

        <Menu.Item
          name='Calender'
          active={activeItem === 'testimonials'}
          onClick={this.handleItemClick}
        >
          Calender
        </Menu.Item>

        <Menu.Menu position='right'>
          <Menu.Item name='signup' active={activeItem === 'signup'} onClick={this.handleItemClick}>
            Sign Up
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    )
  }
}