import React from 'react';
import { Button, Form} from 'semantic-ui-react'

class SearchBox extends React.Component{
	
	constructor(props){
		super(props);
		this.state = {
			searchTerm : ''
		}
	}
	componentDidUpdate(prevProps){
		if(this.props.place != prevProps.place){
			this.setState({
				searchTerm: this.props.place
			})
		}
	}

	changeLocation = (e) =>{

      this.setState({
        searchTerm : e.target.value
      })
  }
	render(){
		return (
			<Form onSubmit={this.onSubmit}>
	            <Form.Field>
	              <label>Search Place</label>
	              <input id="searchbox" placeholder="Enter a location"
	        		value={this.state.searchTerm} onChange={this.changeLocation} />
	            </Form.Field>
	          
	            
	            <Button type='submit'>Seach Experiences</Button>
	        </Form>
		)
	}
}

export default SearchBox;