import React from 'react';
import { connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchActivity} from '../actions/activity_actions';
import { Grid, Image, Button, Label, Header, List, Message } from 'semantic-ui-react'

class SingleActivity extends React.Component{

	constructor(props){
		super(props);
	}

	componentDidMount(){
		if(this.props.match.params.id){
			this.props.fetchActivity(this.props.match.params.id)
		}
	}

	render(){
		const renderCategories = ()=>{
			if(this.props.activity.didReceive){
				const {categoryIds} = this.props.activity.items;
				return categoryIds.map((d,i)=>{
					return (
				      <Label key={i} style={{margin: "5px"}} color='orange' horizontal>
				        {d}
				      </Label>

					)
				})
			}
			else{
				return (<h3>Loading</h3>)
			}
			

		}
		
		const {items,isFetching, didReceive,isError} = this.props.activity; 
		if(isError){
			return (<Message
				    header='Server Error'
				    content='Sorry, There was some error in Server.. Please try after sometime'
				  />)
		}

		else if(!didReceive){
			return (<div>Fetching...</div>)
		}
		
		else{
			return(
				<div>
				<Grid celled>
				    <Grid.Row>
				      <Grid.Column width={3}>
				      	<h3>{items.title}</h3>
				      	<Label color='teal' horizontal>
				        {items.ownerName}
				      </Label>
				      {renderCategories()}
				        <Image src={items.imageUrl} style={{marginBottom : "10px", marginTop : "10px"}} />
				        <Label as='a' color='green' tag>{items.activityTime ? items.activityTime.activityDateString : '' }</Label>
				      </Grid.Column>
				      <Grid.Column width={13}>
				      	<Header size="medium">About the event </Header>
				        <Header as="h5" color="grey">{items.description}</Header>
				      </Grid.Column>
				    </Grid.Row>

				    <Grid.Row>
				      <Grid.Column width={3}>
				        <Button color="blue">{`INR ${items.amount}`}</Button>
				        <h3><Link to="/thanks">Join</Link></h3>
				      </Grid.Column>
				      <Grid.Column width={10}>
				        <Header as="h4">Address</Header>
				        <Header as="h5">{items.location.name +", "+ items.location.locality}</Header>
				      </Grid.Column>
				      <Grid.Column width={3}>
				        <Image src={items.ownerProfileImageUrl} />
				      </Grid.Column>
				    </Grid.Row>
				  </Grid>
				</div>
			)	
		}
		
	}
}
function mapStateToProps(state){
	return{
    	activity : state.activity
  	}
}
export default connect(mapStateToProps, { fetchActivity })(SingleActivity);