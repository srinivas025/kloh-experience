import React from 'react';
import { Card, Message} from 'semantic-ui-react'
import {Link} from 'react-router-dom';

export const RenderCards = (props) => {
	return(
	
		<Card as={Link}
		to={`/activity/${props.activityId}`}
	    image={props.image}
	    header={props.header}
	    meta={props.meta}
	  />
	)
  
}

export default class ActivityList extends React.Component{
	
	
	render(){
		const renderActivities = ()=>{
			const {items} = this.props;
			if(items.length){
				return this.props.items.map((d,i)=>{
					return (
						<RenderCards key={i} activityId= {d.activityId} image={d.imageUrl} header={d.title} meta={d.summary} description={d.description} />
					)
				})
			}
			
		}
		if(!this.props.items.length){
					return (
						<Message
						    header='Not Found'
						    content='Sorry, We do not have any event near you! See you soon!'
						  />
					)
				}
		else{
			return(
				<div>
					<Link to="/" className="align-left"> Back </Link>
					
					<Card.Group itemsPerRow={4}>
						{renderActivities()}
					</Card.Group>

				</div>
			)	
		}
			
	}
	
}