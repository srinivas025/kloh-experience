import React from 'react';
import { connect} from 'react-redux';
import {Message} from 'semantic-ui-react'
import {fetchActivities} from '../actions/activity_actions';
import ActivityList from './activity_list';

class Activities extends React.Component {

	constructor(props){
		super(props);
		this.state = {

		}
	}

	componentDidMount(){
		if(this.props.location.state){
			const {lat, lng} = this.props.location.state.position;
			this.props.fetchActivities(lat, lng);
		}
	}

	render(){

		if(this.props.activities.isError){
			return(
				<Message
				    header='Server Error'
				    content='Sorry, There was some error in Server.. Please try after sometime'
				  />
			)
		}
		
		else if (!this.props.activities.didReceive){
			return (<div>Fetching Activities... </div>)
		}

		else if(this.props.activities.didReceive){
			const {items} = this.props.activities;
			return(
				<div>
					<h1>All Activities near you </h1>
					<ActivityList items = {items} />
				</div>
			)
		}
		
	}
}
function mapStateToProps(state){
  return{
    activities : state.activities
  }
}

export default connect(mapStateToProps, { fetchActivities })(Activities);
