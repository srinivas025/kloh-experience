import React, { Component } from 'react';
import {history} from '../helpers/history';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
import { Button, Form, Grid, Segment,Header} from 'semantic-ui-react';
import SearchBox from './search'

const mapStyles = {
  width: '100%',
  height: '500px'
};

export class Contents extends Component {

state = {
    position: null,
    place : ''
  }

componentDidMount() {
    this.renderAutoComplete();
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps.map) this.renderAutoComplete();
  }

  onSubmit(e) {
    e.preventDefault();
  }

renderAutoComplete() {
    const { google, map } = this.props;

    if (!google || !map) return;

    const autocomplete = new google.maps.places.Autocomplete(document.querySelector('#searchbox'));
    autocomplete.bindTo('bounds', map);

    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();

      if (!place.geometry) return;

      if (place.geometry.viewport) map.fitBounds(place.geometry.viewport);
      else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }

      this.setState({ position: place.geometry.location, place:place.formatted_address },
        function(){
          this.props.history.push({
            pathname: '/activities',
            state: { position: {lat:this.state.position.lat(), lng: this.state.position.lng()}}
          })    
      });
      
    });
  }

  changeLocation = (e) =>{
    if(e.target.value){
      this.setState({
        searchTerm : e.target.value
      })
    }
  }
  render() {
    const { position } = this.state;
        return (
          <div className="">
          <Header as='h1' color="olive">Find Experiences Near You!</Header>
            <Grid columns={2}>
              <Grid.Row stretched>
                <Grid.Column width={3}>
                  <Segment>
                  
                  <SearchBox place={this.state.place}/>
                  </Segment>
                </Grid.Column>
                <Grid.Column width={13}>
                  <Map
                    {...this.props}
                    center={position}
                    centerAroundCurrentLocation={false}
                    containerStyle={{
                      height: '100vh',
                      position: 'relative',
                      width: '100%'
                    }}>
                    <Marker position={position} />
                  </Map>
                </Grid.Column>
              </Grid.Row>

            </Grid>
            
            
          </div>
    );
  }
}

const MapWrapper = props => (
  <Map className="map" google={props.google} visible={false}>
    <Contents {...props} />
  </Map>
);

export default GoogleApiWrapper({
  apiKey: 'AIzaSyBNc9XHFq1Pde8tnh8448-aA5mg2oW5haU'
})(MapWrapper);