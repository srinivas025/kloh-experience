import axios from 'axios';
export const FETCH_ACTIVITIES = "FETCH_ACTIVITIES";
export const REQUEST_ACTIVITIES = "REQUEST_ACTIVIRIES";
export const RECEIVE_ACTIVITIES = "RECEIVE_ACTIVITIES";
export const ERROR_ACTIVITIES = "ERROR_ACTIVITIES";

export const FETCH_ACTIVITY = "FETCH_ACTIVITY";
export const REQUEST_ACTIVITY = "REQUEST_ACTIVITY";
export const RECEIVE_ACTIVITY = "RECEIVE_ACTIVITY";
export const ERROR_ACTIVITY = "ERROR_ACTIVITY";

function requestActivities() {
  return {
    type: REQUEST_ACTIVITIES,
    payload : "Requesting"
  }
}

function receiveActivities(data) {
  return {
    type: RECEIVE_ACTIVITIES,
    payload :data

  }
}
function errorActivities(data) {
  return {
    type: ERROR_ACTIVITIES,
    payload :data

  }
}

function requestActivity() {
  return {
    type: REQUEST_ACTIVITY,
    payload : "Requesting"
  }
}

function receiveActivity(data) {
  return {
    type: RECEIVE_ACTIVITY,
    payload :data

  }
}
function errorActivity(data) {
  return {
    type: ERROR_ACTIVITY,
    payload :data

  }
}


export function fetchActivities(lat,lng) {
  return function(dispatch) {
    dispatch(requestActivities())
    return axios.post('https://api.kloh.in/kloh/external/v1/activity/list',
		{
		"location": {
			"lat": lat,
			"lon": lng
		}

		}).then(function(res){
			dispatch(receiveActivities(res.data.response.results))
		})
		.catch(function (error) {
		    console.log(error);
		    dispatch(errorActivities(error))
		});
  }
}

export function fetchActivity(id) {
  return function(dispatch) {
    dispatch(requestActivity())
    return axios.get(`https://api.kloh.in/kloh/external/v1/activity/${id}`
		).then(function(res){
			dispatch(receiveActivity(res.data.response))
		})
		.catch(function (error) {
		    dispatch(errorActivity(error))
		});
  }
}
