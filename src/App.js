import React, { Component } from 'react';
import {Router, Route, Switch} from 'react-router-dom';
import './App.css';
import MapWrapper from './components/maps';
import Activities from './components/activities';
import SingleActivity from './components/activity_single';
import MenuBar from './components/header';
import {Thanks} from './components/thanks';
import { Header } from 'semantic-ui-react'
import {history} from './helpers/history';

class App extends Component {
  render() {
    return (
      <div className="App ui container">
        <header className="App-header">
          <Router history={history} >
          <div className="wrapper">
            <MenuBar />
            <Switch>
            <Route exact path='/' component = {MapWrapper}  />
            <Route exact path='/activities' component = {Activities}  />
            <Route exact path='/activity/:id' component = {SingleActivity}  />
            <Route exact path='/thanks' component = {Thanks}  />
            </Switch>
          </div>
        </Router> 
        </header>
      </div>
    );
  }
}

export default App;
