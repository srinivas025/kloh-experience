import {FETCH_ACTIVITIES, REQUEST_ACTIVITIES, RECEIVE_ACTIVITIES,ERROR_ACTIVITIES} from '../actions/activity_actions'
export default function activities(
  state = {
    isFetching: false,
    didReceive : false,
    isError:false,
    error:{},
    items: []
  },
  action
) {
  switch (action.type) {

    case REQUEST_ACTIVITIES:
      return Object.assign({}, state, {
        isFetching: true,
        didReceive: false
      })
    case ERROR_ACTIVITIES:
      return Object.assign({}, state, {
        isFetching: false,
        didReceive: false,
        isError:true,
        error: action.payload
      })
    case RECEIVE_ACTIVITIES:
      return Object.assign({}, state, {
        isFetching: false,
        didReceive:true,
        items: action.payload,
      })
    default:
      return state
  }
}