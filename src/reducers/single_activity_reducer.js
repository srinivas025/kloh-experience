import {FETCH_ACTIVITY, REQUEST_ACTIVITY, RECEIVE_ACTIVITY, ERROR_ACTIVITY} from '../actions/activity_actions'
export default function activity(
  state = {
    isFetching: false,
    didReceive : false,
    isError:false,
    error:{},
    items: {}
  },
  action
) {
  switch (action.type) {

    case REQUEST_ACTIVITY:
      return Object.assign({}, state, {
        isFetching: true,
        didReceive: false
      })
    case ERROR_ACTIVITY:
      return Object.assign({}, state, {
        isFetching: false,
        didReceive: false,
        isError:true,
        error:action.payload

      })
    case RECEIVE_ACTIVITY:
      return Object.assign({}, state, {
        isFetching: false,
        didReceive:true,
        items: action.payload,
      })
    default:
      return state
  }
}