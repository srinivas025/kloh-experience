import { combineReducers } from 'redux';
import activities from './activities_reducer';
import activity from './single_activity_reducer';

const rootReducer = combineReducers({
  activities: activities,
  activity:activity
});

export default rootReducer;
